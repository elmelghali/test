FROM selenium/standalone-chrome
# Install chromedriver for Selenium
RUN curl https://chromedriver.storage.googleapis.com/75.0.3770.140/chromedriver_linux64.zip -o /usr/local/bin/chromedriver
RUN chmod +x /usr/local/bin/chromedriver

WORKDIR /
ADD selenium.jar selenium.jar
EXPOSE 8080
CMD java -jar selenium.jar
