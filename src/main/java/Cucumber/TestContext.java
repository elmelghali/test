package Cucumber;

import Utils.PageObjectManager;
import Utils.SeleniumDriver;
import Utils.WebDriverManager;

public class TestContext {
    public ScenarioContext scenarioContext;
    private WebDriverManager webDriverManager;
    private PageObjectManager pageObjectManager;

    public static int nbr   =   0;

    public TestContext(){
        webDriverManager    =   new WebDriverManager();
        pageObjectManager   =   new PageObjectManager(webDriverManager.getDriver());
        scenarioContext     =   new ScenarioContext();


        System.out.println("run "+nbr);
        nbr++;
    }

    public WebDriverManager getWebDriverManager() { return webDriverManager; }

    public PageObjectManager getPageObjectManager() { return pageObjectManager; }

    public ScenarioContext getScenarioContext() { return scenarioContext; }
}
