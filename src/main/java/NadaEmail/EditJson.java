package NadaEmail;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import gherkin.deps.com.google.gson.*;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.json.simple.JSONArray;

import java.io.*;

public class EditJson {
    public static void plainAppendExample(JSONObject jsonObject) {

        try {
            File file = new File(System.getProperty("user.dir") + "\\src\\main\\java\\NadaEmail\\email.json");
            ObjectMapper mapper = new ObjectMapper();
            JsonGenerator g = mapper.getFactory().createGenerator(new FileOutputStream(file));
            //Object o = [""];
            JSONObject obj = (JSONObject) new JSONObject().get("emails");
            obj.put("Name", "crunchify.com");
            obj.put("Author", "App Shah");

            mapper.writeValue(g, jsonObject);
            mapper.writeValue(g, ",");
            mapper.writeValue(g, obj);
            g.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public static void add() throws IOException, ParseException {
        JSONObject jsonObject   =   null;
        File file = new File(System.getProperty("user.dir") + "\\src\\main\\java\\NadaEmail\\email.json");
        Gson gson = new Gson();
        JSONParser parser = new JSONParser();
        Object obj = parser.parse(new FileReader(System.getProperty("user.dir")+"\\src\\main\\java\\NadaEmail\\email.json"));
        //JsonObject inputObj  = gson.fromJson(obj.toString(), JsonObject.class);
        JSONObject inputObj = (JSONObject) obj;

        System.out.println(obj);
        //System.out.println(inputObj.get("emails"));
        JSONArray arrayObj = (JSONArray) inputObj.get("emails");

        System.out.println(arrayObj);

        JsonObject newObject = new JsonObject() ;
        newObject.addProperty("lat", "newValue");
        newObject.addProperty("lon", "newValue");
        //inputObj.get("emails").getAsJsonArray().add(newObject);
        arrayObj.add(newObject);
        System.out.println(arrayObj);

        JSONObject finalObj = new JSONObject();
        finalObj.put("emails",arrayObj);
        System.out.println(finalObj);

        //System.out.println(inputObj);
    }

    public static void AddAnotherEmail() throws IOException, ParseException {


        //parse json file
        JSONParser parser = new JSONParser();
        Object obj = parser.parse(new FileReader(System.getProperty("user.dir")+"\\src\\main\\java\\NadaEmail\\email.json"));
        JSONObject inputObj = (JSONObject) obj;

        JSONArray arrayObj = (JSONArray) inputObj.get("emails");

        JsonObject newObject = new JsonObject() ;
        newObject.addProperty("lat", "newValue");
        newObject.addProperty("lon", "newValue");
        arrayObj.add(newObject);

        //prepare a new object
        JSONObject finalObj = new JSONObject();
        finalObj.put("emails",arrayObj);
        System.out.println(finalObj);
/*
        File file = new File(System.getProperty("user.dir") + "\\src\\main\\java\\NadaEmail\\email.json");
        ObjectMapper mapper = new ObjectMapper();
        JsonGenerator g = mapper.getFactory().createGenerator(new FileOutputStream(file));

        mapper.writeValue(g, finalObj);
        g.close();
*/

        //JSONObject obj = (JSONObject) new JSONObject().get("emails");
    }

    public static JSONObject getFinalJsonObject() throws IOException, ParseException {
        //parse json file
        JSONParser parser = new JSONParser();
        Object obj = parser.parse(new FileReader(System.getProperty("user.dir")+"\\src\\main\\java\\NadaEmail\\email.json"));
        JSONObject inputObj = (JSONObject) obj;

        JSONArray arrayObj = (JSONArray) inputObj.get("emails");

        JsonObject newObject = new JsonObject() ;
        newObject.addProperty("lat", "fergfgd");
        newObject.addProperty("lon", "sdffggfsgfsfgfg");
        arrayObj.add(newObject);

        //prepare a new object
        JSONObject finalObj = new JSONObject();
        finalObj.put("emails",arrayObj);
        //System.out.println(finalObj);
        return finalObj;
    }

    public static void writeFinalObj(JSONObject jsonObject) throws IOException {
        /*
        ObjectMapper myObjectMapper = new ObjectMapper();
        myObjectMapper.enable(SerializationFeature.INDENT_OUTPUT);
        myObjectMapper.writeValue(new File(System.getProperty("user.dir") + "\\src\\main\\java\\NadaEmail\\email.json"), jsonObject.toJSONString());
*/
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        JsonParser jp = new JsonParser();
        JsonElement je = jp.parse(jsonObject.toString());
        String prettyJsonString = gson.toJson(je);
       // try {
/*
            JsonParser jsonParser = new JsonParser();
            JsonObject jo = (JsonObject)jsonParser.parse(jsonObject.toString());

            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            String json = gson.toJson(jsonObject);
            System.out.println(json);
            */


            try (FileWriter file = new FileWriter(System.getProperty("user.dir") + "\\src\\main\\java\\NadaEmail\\email.json")) {
                file.write(prettyJsonString);
                System.out.println("Successfully Copied JSON Object to File...");
                System.out.println("\nJSON Object: " + prettyJsonString);
            }


/*
            File file = new File(System.getProperty("user.dir") + "\\src\\main\\java\\NadaEmail\\email.json");
            ObjectMapper mapper = new ObjectMapper();
            JsonGenerator g = mapper.getFactory().createGenerator(new FileOutputStream(file));
            mapper.writeValue(g, jsonObject);
            g.close();

        } catch (IOException e) {
            e.printStackTrace();
        }*/
    }
}
