package NadaEmail;


import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.restassured.config.HttpClientConfig;
import io.restassured.config.RestAssuredConfig;
import io.restassured.path.json.exception.JsonPathException;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;
import org.apache.commons.lang3.RandomStringUtils;

import io.restassured.RestAssured;
import org.apache.http.params.CoreConnectionPNames;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import java.net.ServerSocket;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static io.restassured.RestAssured.config;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.hasKey;


//import org.awaitility.Duration;

//import org.awaitility.Awaitility.await;


public class  NadaEMailService {

    private static final String NADA_EMAIL_DOMAIN = "@zetmail.com";
    private static final String ENDTEST_EMAIL_DOMAIN = "@endtest-mail.io";
    private static final String INBOX_MESSAGE_KEY_NAME = "msgs";
    private static final String EMAIL_ID_ROUTE_PARAM = "email-id";
    private static final String MESSAGE_ID_ROUTE_PARAM = "message-id";
    private static final String NADA_EMAIL_INBOX_API = "https://getnada.com/api/v1/inboxes/{email-id}";
    private static final String NADA_EMAIL_MESSAGE_API = "https://getnada.com/api/v1/messages/{message-id}";
    private static final ObjectMapper MAPPER = new ObjectMapper();
    private static final int EMAIL_CHARS_LENGTH = 4;

    private String emailId;


    private void generateEmailId(){
        this.emailId = "wintestbot"+RandomStringUtils.randomAlphanumeric(EMAIL_CHARS_LENGTH).toLowerCase().concat(ENDTEST_EMAIL_DOMAIN);
        System.out.println("win"+emailId);
        //this.emailId = RandomStringUtils.randomAlphanumeric(EMAIL_CHARS_LENGTH).toLowerCase();
    }

    //generates a random email for the first time.
    //call reset for a new random email
    public String getEmailId(){
        if(Objects.isNull(this.emailId)){
            this.generateEmailId();
        }
        System.out.println(this.emailId);
        return this.emailId;
    }
/*
    public String getMessages(String emailID){
        RestAssured.baseURI = "https://getnada.com/api/v1/inboxes";
        RequestSpecification httpRequest = RestAssured.given();
        //Response response = httpRequest.get("/8editx9mvj@nada.ltd");
        Response response = httpRequest.get("/"+emailID);
        ResponseBody body = response.getBody();
        String uid  =   response.getBody().jsonPath().get("msgs[0].uid");
        System.out.println(uid);
        return body.prettyPrint();
    }
*/
    public String getUidMessage1(String email){
        System.out.println("getting UID");
        System.out.println(given().when().get("https://getnada.com/api/v1/inboxes/"+email).then().body("$",hasKey("msgs[0].uid")));
        return given().when().get("https://getnada.com/api/v1/inboxes/"+email).then().body("$",hasKey("msgs[0].uid")).toString();

    }
    public String getUidMessage(String email) throws InterruptedException {
        //RestAssured.baseURI = "https://getnada.com/api/v1/inboxes";
        //Thread.sleep(1000);
        boolean isfound = false;
        String uid =    null;
        /*
        RequestSpecification httpRequest = RestAssured.given();
        Response response = httpRequest.get("/"+email);
        uid =   response.getBody().jsonPath().get("msgs[0].uid").toString();
        */

        while(isfound == false){
            //Thread.sleep(1000);
            RestAssured.baseURI = "https://getnada.com/api/v1/inboxes";
            //obj = response.getBody().jsonPath().get("msgs[0].uid");
            //System.out.println("this my uid below :");
            try {
                RequestSpecification httpRequest = given();
                Response response = httpRequest.get("/"+email);
                System.out.println("UID : waiting for validation Link of email : "+this.getEmailId());
                //System.out.println("uid "+response.getBody().jsonPath().get("msgs[0].uid").toString());
                System.out.println(response.getBody().jsonPath().get("msgs[0].uid").toString());
                if(!response.getBody().jsonPath().get("msgs[0].uid").equals(null)){
                    System.out.println("uid is found");
                    isfound = true;
                    return response.getBody().jsonPath().get("msgs[0].uid").toString();
                }

            }catch (JsonPathException e){
                e.printStackTrace();
            }catch(NullPointerException ex){
                ex.printStackTrace();
            }
            /*
            System.out.printlthis my uid belown("uid "+response.getBody().jsonPath().get("msgs[0].uid").toString());
            if(response.getBody().jsonPath().get("msgs[0].uid")!=null){
                isfound = true;
                uid     =   response.getBody().jsonPath().get("msgs[0].uid").toString();
            }else{
                isfound =   false;
            }
            System.out.println("UID : waiting for validation Link");
            System.out.println("UID : "+uid);
            System.out.println("Email: "+this.getEmailId()+" UID : " +uid);
            */
        }
        return uid;
    }

    public String getVerificationCode(String UID) throws InterruptedException, IOException {
        //RestAssured.baseURI = "https://getnada.com/api/v1/messages/";
        ServerSocket serverSocket = null;
        String codeDeVerification = "";
        try {

            serverSocket = new ServerSocket(3333);
            serverSocket.setSoTimeout(3000);
            String bURL = "https://endtest.io/mailbox?format=json&email=".concat(UID);
            RestAssuredConfig config = RestAssured.config()
                    .httpClient(HttpClientConfig.httpClientConfig().
                                    setParam("http.connection.timeout",300000).
                                    setParam("http.socket.timeout",300000).
                                    setParam("http.connection-manager.timeout",300000));
                            //.setParam(CoreConnectionPNames.CONNECTION_TIMEOUT, 20000)
                            //.setParam(CoreConnectionPNames.SO_TIMEOUT, 20000));


            RequestSpecification httpRequest = given().config(config);
            Thread.sleep(10000);
            Response response = httpRequest.get(bURL);
            //Thread.sleep(30000);
            //String  html        =   response.getBody().jsonPath().get("html");
            String html = response.getBody().jsonPath().getString("html");
            Pattern p = Pattern.compile(">(\\d{4})<");
            Matcher m = p.matcher(html);
            System.out.println("matcher ==> " + m);
            if (m.find()) {
                codeDeVerification = m.group(1);
                System.out.println(codeDeVerification);
            }

        } catch (SocketTimeoutException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (serverSocket != null)
                    serverSocket.close();

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        return codeDeVerification;
    }
    public String getTheVerificationCode(String html){
        //Pattern p = Pattern.compile("href=\"(.*?)\"");
        Pattern p  =   Pattern.compile(">(\\d{4})<");
        Matcher m = p.matcher(html);

        System.out.println("matcher ==> "+m);
        String codeDeVerification="";

        if (m.find()) {
            codeDeVerification = m.group(1); // this variable should contain the link URL
            System.out.println(codeDeVerification);
        }
        return codeDeVerification;
    }

    public String getValidationLinkProd(String html){
        //Pattern p = Pattern.compile("href=\"(.*?)\"");
        Pattern p  =   Pattern.compile("href=\"https://win.ma/inscription/(.*?)\"");
        Matcher m = p.matcher(html);

        System.out.println("matcher ==> "+m);
        String url = null;



        if (m.find()) {

            url = "https://win.ma/inscription/"+m.group(1); // this variable should contain the link URL
            System.out.println("url ==> "+url);
            //System.out.println(url);
            System.out.println("goup count "+m.groupCount());
        }
        return url.replace("&amp;","&");
    }
/*
    public void AddAnEmail(){
        JSONObject obj = new JSONObject();
        obj.put("Name", "crunchify.com");
        obj.put("Author", "App Shah");

        JSONParser parser = new JSONParser();
        try {
            Object myObj = parser.parse(new FileReader(System.getProperty("user.dir")+"\\src\\main\\java\\NadaEmail\\email.json"));
            //JSONObject jsonObject = (JSONObject) myObj;
            System.out.println(myObj);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
*/

    public void addObject(){
        JSONObject obj = new JSONObject();
        obj.put("emailId", "kjkhjh@mkyong.com");
        obj.put("link", "www.dfjlkfjjf.//fdjlfkj");
        try (FileWriter file = new FileWriter(System.getProperty("user.dir")+"\\src\\main\\java\\NadaEmail\\email.json")) {

            file.write(obj.toString());
            file.flush();

        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.print(obj);
    }

    public JSONObject ReadEmails(){
        JSONParser parser = new JSONParser();
        JSONObject jsonObject   =   null;

        try {

            Object obj = parser.parse(new FileReader(System.getProperty("user.dir")+"\\src\\main\\java\\NadaEmail\\email.json"));

             jsonObject = (JSONObject) obj;
            //System.out.println(jsonObject);

            String name = (String) jsonObject.get("emailId");
            //System.out.println(name);

            String age = (String) jsonObject.get("link");
            //System.out.println(age);


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    public void AddAnEmail(JSONObject emails,String email,String link){
        emails.put("emailId", email);
        emails.put("link", link);
        try (FileWriter file = new FileWriter(System.getProperty("user.dir")+"\\src\\main\\java\\NadaEmail\\email.json")) {

            file.write(emails.toString());
            file.flush();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}