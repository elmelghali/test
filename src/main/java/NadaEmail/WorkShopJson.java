package NadaEmail;

import gherkin.deps.com.google.gson.*;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.*;

public class WorkShopJson {

    public  JSONObject getFinalJsonObject(String email,String link) throws IOException, ParseException {
        //parse json file
        JSONParser parser = new JSONParser();
        Object obj = parser.parse(new FileReader(System.getProperty("user.dir")+"\\src\\main\\java\\NadaEmail\\email.json"));
        JSONObject inputObj = (JSONObject) obj;

        JSONArray arrayObj = (JSONArray) inputObj.get("emails");

        JsonObject newObject = new JsonObject() ;
        newObject.addProperty("email", email);
        newObject.addProperty("link", link);
        arrayObj.add(newObject);

        //prepare a new object
        JSONObject finalObj = new JSONObject();
        finalObj.put("emails",arrayObj);
        //System.out.println(finalObj);
        return finalObj;
    }
    public  void printPrettyJson(JSONObject jsonObject){
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        JsonParser jp = new JsonParser();
        JsonElement je = jp.parse(jsonObject.toString());
        String prettyJsonString = gson.toJson(je);

        try (FileWriter file = new FileWriter(System.getProperty("user.dir") + "\\src\\main\\java\\NadaEmail\\email.json")) {
            file.write(prettyJsonString);
            System.out.println("Successfully Copied JSON Object to File...");
            //System.out.println("\nJSON Object: " + prettyJsonString);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String b2UrlEncode(String s) throws UnsupportedEncodingException {
        return java.net.URLEncoder.encode(s, "UTF-8").replace("%2F", "/");
    }

    public  String getLLastEmail() throws IOException, ParseException {
        JSONParser parser = new JSONParser();
        Object obj = parser.parse(new FileReader(System.getProperty("user.dir")+"\\src\\main\\java\\NadaEmail\\email.json"));
        JSONObject inputObj = (JSONObject) obj;

        JSONArray arrayObj = (JSONArray) inputObj.get("emails");
        int size    =   arrayObj.size()-1;
        //JSONArray myArrayObj = (JSONArray) inputObj.get("emails[33].email");

        String myEmail  =   arrayObj.get(size).toString();
        //JSONArray anObj    =  (JSONArray) arrayObj.get(size-1);
        System.out.println(myEmail);

       // Object myObj = parser.parse(anObj.toString());
       // System.out.println(myObj);

        //JSONObject site = (JSONObject)(((JSONArray)arrayObj.get(0)).get(0));
        //JSONObject jsonObject = new JSONObject(myEmail);
        JSONObject object = (JSONObject) JSONValue.parse(myEmail);
        System.out.println(object.get("email"));
        return object.get("email").toString();
    }
}
