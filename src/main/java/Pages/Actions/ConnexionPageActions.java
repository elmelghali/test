package Pages.Actions;

import Pages.Locators.ConnexionLocators;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ConnexionPageActions {

    ConnexionLocators connexionPage = new ConnexionLocators() ;
    WebDriver driver ;
    WebDriverWait wait;
    Actions action;

    public ConnexionPageActions(WebDriver driver){
        this.driver         =   driver;
        this.connexionPage  =   new ConnexionLocators();
        action= new Actions(driver);
        this.wait           =   new WebDriverWait(driver,90);
        PageFactory.initElements(driver, connexionPage);

    }
    public void setUserName(String userName){

        WebElement emailElement=connexionPage.email;
        action.moveToElement(emailElement).click().perform();
        action.moveToElement(emailElement).sendKeys(userName);
    }

    public void setPassword(String password){
        WebElement passwordElement=connexionPage.password;
        action.moveToElement(passwordElement).click().perform();
        action.moveToElement(passwordElement).sendKeys(password);
    }

    public void connexion(){
        action.moveToElement(connexionPage.connexion).click().perform();
    }



}
