package Pages.Actions.P1;

import Pages.Locators.P1.P1Locators;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class P1Actions {
    WebDriver driver ;
    WebDriverWait wait;
    Actions action;
    P1Locators P1Page;

    public P1Actions(WebDriver driver){
        this.driver         =   driver;
        this.P1Page  =   new P1Locators();
        action= new Actions(driver);
        this.wait           =   new WebDriverWait(driver,90);
        PageFactory.initElements(driver, P1Page);

    }

    public void clickOnSuiviExecutionProject() throws InterruptedException {
        Thread.sleep(500);
        action.moveToElement(P1Page.suiviExecutionProject).click().perform();
        Thread.sleep(1000);

    }

    public void clickOnProgramme1Link() throws InterruptedException {
        Thread.sleep(1000);
        action.moveToElement(P1Page.programme1Link).click().perform();
    }

    public Boolean checkIfTheTableIsEmpty(){
       List<WebElement> tableContent= driver.findElements(By.xpath(P1Page.getContenuTableau()));
       return tableContent.isEmpty()?true:false;
    }

    public void searchForaValidatedProject() throws InterruptedException {
        action.moveToElement(P1Page.statutProjetFilter).click().perform();
        Thread.sleep(1000);
        action.moveToElement(P1Page.statutProjetFilter).click().perform();
        Thread.sleep(1000);
        action.moveToElement(P1Page.valideCPDH_Statut).click().perform();
        Thread.sleep(1000);
        action.moveToElement(P1Page.filtrerButton).click().perform();
        Thread.sleep(1000);
    }
    public void iConsultTheFirstProjectOfTheList(){

        //To check that the table contains at least one project
        Assert.assertFalse(false);
        action.moveToElement(P1Page.visualiserButton).click().perform();

    }
    public void iCheckTheStatusOfTheProject() throws InterruptedException {
        Thread.sleep(1000);
        Assert.assertEquals("Validé CPDH",P1Page.statusProjectLabel.getText());
        Thread.sleep(1000);
    }

    public void create_a_PhysicalFollowUpSheet() throws InterruptedException {
        Thread.sleep(1000);
        List<WebElement> buttonNouvelleFicheDeSuiviPhysiqueIsDisplayed= driver.findElements(By.xpath(P1Page.getNouvelleFicheDeSuiviPhysiqueLocator()));
        if(buttonNouvelleFicheDeSuiviPhysiqueIsDisplayed.size()!=0)
          action.moveToElement(P1Page.nouvelleFicheDeSuiviPhysique).click().perform();
        else {
            action.moveToElement(P1Page.troisPointButton).click().perform();
            action.moveToElement(P1Page.nouvelleSituationButton).click().perform();
        }
        Thread.sleep(1000);

        action.moveToElement(P1Page.tauxAvancementInput).click().perform();
        action.moveToElement(P1Page.tauxAvancementInput).sendKeys("7.9");
        Thread.sleep(1000);

        action.moveToElement(P1Page.nombreUniteRealiseInput).click().perform();
        action.moveToElement(P1Page.nombreUniteRealiseInput).sendKeys("50");
        Thread.sleep(1000);

        action.moveToElement(P1Page.observationTextArea).click().perform();
        action.moveToElement(P1Page.observationTextArea).sendKeys("Premier avancement du projet");
        Thread.sleep(1000);

        action.moveToElement(P1Page.sauvegarderButton).click().perform();
    }










}

