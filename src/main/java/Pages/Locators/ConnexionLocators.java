package Pages.Locators;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class ConnexionLocators {

    static public final String emailLocator =   "//input[@id='username']";
    static public final String passwordLocator =   "//input[@id='password']";
    static public final String connexionLocator      =   "//input[@id='kc-login']";



    @FindBy(how= How.XPATH,using= emailLocator)
    public WebElement email;


    @FindBy(how=How.XPATH,using= passwordLocator)
    public WebElement password;

    @FindBy(how=How.XPATH,using= connexionLocator)
    public WebElement connexion;


}
