package Pages.Locators.P1;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class P1Locators {

    static public final String suiviExecutionProjectLocator =                   "//a[@href='#/suivi-execution']/span";
    static public final String programme1Locator =                              "//a[@href='#/suivi-execution/programme1']/span";
    static public final String contenuTableau =                                 "//tbody/tr";
    static public final String statutProjetFilterLocator =                      "//ng-select[@id='field_statutProjet']/div/div/div[2]/input";
    static public final String valideCPDH_StatutLocator =                       "//ng-dropdown-panel/div/div[2]/div[17]/span";
    static public final String filtrerButtonLocator =                           "//form[@name='searchForm']/div/div[9]/button[1]";
    static public final String visualiserButtonLocator =                        "//div[@id='entities']/table/tbody/tr[1]/td[@class='action']/button";
    static public final String statusProjectLabelLocator =                      "//app-consulter-projet-p1/div/h3/span[2]";
    static public final String nouvelleFicheDeSuiviPhysiqueLocator =            "//app-consulter-projet-p1/div/h3/div/button[@class='btn btn-primary']";
    static public final String tauxAvancementInputLocator =                     "//input[@id='tauxAvancement']";
    static public final String nombreUniteRealiseInputLocator =                 "//input[@id='nombreUniteRealise']";
    static public final String observationTextAreaLocator =                     "//textarea[@id='observations']";
    static public final String sauvegarderButtonLocator =                       "//form[@name='myForm']/div/div[3]/button/span[text()='Sauvegarder']";
    static public final String troisPointButtonLocator =                        "//button[@class='dropdown-toggle three-dots']/i[@class='icon-three-dots']";
    static public final String nouvelleSituationButtonLocator =                 "//span[@apptranslate='suiviExecution.ficheSuivi.nouvelle_situation']";






    @FindBy(how= How.XPATH,using= suiviExecutionProjectLocator)
    public WebElement suiviExecutionProject;

    @FindBy(how=How.XPATH,using= programme1Locator)
    public WebElement programme1Link;

    @FindBy(how=How.XPATH,using= statutProjetFilterLocator)
    public WebElement statutProjetFilter;

    @FindBy(how=How.XPATH,using= valideCPDH_StatutLocator)
    public WebElement valideCPDH_Statut;

    @FindBy(how=How.XPATH,using= filtrerButtonLocator)
    public WebElement filtrerButton;

    @FindBy(how=How.XPATH,using= visualiserButtonLocator)
    public WebElement visualiserButton;

    @FindBy(how=How.XPATH,using= statusProjectLabelLocator)
    public WebElement statusProjectLabel;

    @FindBy(how=How.XPATH,using= nouvelleFicheDeSuiviPhysiqueLocator)
    public WebElement nouvelleFicheDeSuiviPhysique;

    @FindBy(how=How.XPATH,using= tauxAvancementInputLocator)
    public WebElement tauxAvancementInput;

    @FindBy(how=How.XPATH,using= nombreUniteRealiseInputLocator)
    public WebElement nombreUniteRealiseInput;

    @FindBy(how=How.XPATH,using= observationTextAreaLocator)
    public WebElement observationTextArea;

    @FindBy(how=How.XPATH,using= sauvegarderButtonLocator)
    public WebElement sauvegarderButton;

    @FindBy(how=How.XPATH,using= troisPointButtonLocator)
    public WebElement troisPointButton;

    @FindBy(how=How.XPATH,using= nouvelleSituationButtonLocator)
    public WebElement nouvelleSituationButton;




    public  String getContenuTableau() {
        return contenuTableau;
    }

    public  String getNouvelleFicheDeSuiviPhysiqueLocator() {
        return nouvelleFicheDeSuiviPhysiqueLocator;
    }
}
