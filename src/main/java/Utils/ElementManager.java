package Utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ElementManager {

    WebDriver   driver;
    WebDriverWait wait;

    public  ElementManager(WebDriver driver){
        this.driver     =   driver;
        this.wait       =   new WebDriverWait(driver, 6);

    }

    public  boolean isElementClickable(WebElement el){
        try{
            wait.until(ExpectedConditions.elementToBeClickable(el));
            el.click();
            System.out.println("from element is clickable method  ====>  yes ");
            return true;
        }
        catch (Exception e){
            //e.printStackTrace();
            return false;
        }
    }
}
