package Utils.ExtentReports;

import com.cucumber.listener.ExtentCucumberFormatter;
//import com.relevantcodes.extentreports.ExtentReports;
//import com.vimalselvam.cucumber.listener.ExtentCucumberFormatter;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class ExtentManager {
    //private static ExtentReports extent;
    private static File newFile;

    public static File getFile(){
        if(newFile == null){
            // Initiates the extent report and generates the output in the output/Run_<unique timestamp>/report.html file by default.

            SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy_hhmmss");
            //ExtentCucumberFormatter.

            Date curDate = new Date();
            String strDate = sdf.format(curDate);
            String fileName = System.getProperty("user.dir")+"\\target\\Extent_Reports\\" + strDate+"win.html";
            newFile = new File(fileName);
            //ExtentCucumberFormatter.initiateExtentCucumberFormatter(newFile,false);
        }
        return newFile;
    }

    public synchronized static void getReporter(){
        /*
        if(extent == null){
            //Set HTML reporting file location
            String workingDir = System.getProperty("user.dir");
            extent = new ExtentReports(workingDir+"\\ExtentReports\\ExtentReportResults.html", true);
        }
        return extent;
*/

        if(newFile == null){
            // Initiates the extent report and generates the output in the output/Run_<unique timestamp>/report.html file by default.

            SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy_hhmmss");
            //ExtentCucumberFormatter.

            Date curDate = new Date();
            String strDate = sdf.format(curDate);
            String fileName = System.getProperty("user.dir")+"\\target\\Extent_Reports\\" + strDate+"noor150.html";
            newFile = new File(fileName);
            /*
            ExtentCucumberFormatter.initiateExtentCucumberFormatter(newFile,false);
            // static report name
            // ExtentCucumberFormatter.initiateExtentCucumberFormatter(new File("F:\\cucumber-testing-master\\ExtenReports\\extentreports.html"),false);
            // Loads the extent config xml to customize on the report.
            ExtentCucumberFormatter.loadConfig(new File("src/test/resources/extent-config.xml"));
            // User can add the system information as follows
            ExtentCucumberFormatter.addSystemInfo("Platform", "Web 2.9");
            ExtentCucumberFormatter.addSystemInfo("Browser Name", "Chrome");
            ExtentCucumberFormatter.addSystemInfo("Browser version", "v57.0");
            ExtentCucumberFormatter.addSystemInfo("Selenium version", "v3.9.1");

            // Also you can add system information using a hash map
            Map systemInfo = new HashMap();
            systemInfo.put("Cucumber version", "v1.2.3");
            systemInfo.put("Extent Cucumber Reporter version", "v1.1.0");
            ExtentCucumberFormatter.addSystemInfo(systemInfo);
            */


        }

        // return extent;

    }

    public synchronized static void generateReport(){
        //ExtentCucumberFormatter.initiateExtentCucumberFormatter(newFile,false);
        // static report name
        // ExtentCucumberFormatter.initiateExtentCucumberFormatter(new File("F:\\cucumber-testing-master\\ExtenReports\\extentreports.html"),false);
        // Loads the extent config xml to customize on the report.
        /*
        ExtentCucumberFormatter.loadConfig(new File("src/test/resources/extent-config.xml"));
        // User can add the system information as follows
        ExtentCucumberFormatter.addSystemInfo("Platform", "Web 2.9");
        ExtentCucumberFormatter.addSystemInfo("Browser Name", "Chrome");
        ExtentCucumberFormatter.addSystemInfo("Browser version", "v57.0");
        ExtentCucumberFormatter.addSystemInfo("Selenium version", "v3.9.1");

        // Also you can add system information using a hash map
        Map systemInfo = new HashMap();
        systemInfo.put("Cucumber version", "v1.2.3");
        systemInfo.put("Extent Cucumber Reporter version", "v1.1.0");
        ExtentCucumberFormatter.addSystemInfo(systemInfo);
        */

    }
}
