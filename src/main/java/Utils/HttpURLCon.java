package Utils;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;


public class HttpURLCon {

    private final String USER_AGENT = "Mozilla/5.0";

    public String sendGet(String cartID) throws Exception {

        String url = "https://backend-digital.recette.noor.ma/api/v2/sms/verification-code?"+cartID;
        URL obj = new URL(url);
        String rep;
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        //Request header
        con.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 5.1; rv:19.0) Gecko/20100101 Firefox/19.0");
        //int responseCode = con.getResponseCode();
        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        StringBuffer OTP = new StringBuffer();
        while ((rep = in.readLine()) != null) {
            OTP.append(rep);
        }
        in.close();
        return OTP.toString();
    }
}
