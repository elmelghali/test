package Utils;

import NadaEmail.NadaEMailService;
import NadaEmail.WorkShopJson;
import Pages.Actions.ConnexionPageActions;
import Pages.Actions.P1.P1Actions;
import org.openqa.selenium.WebDriver;
import org.apache.commons.lang3.time.StopWatch;

public class PageObjectManager {

    private NadaEMailService                nadaEmail;
    private ElementManager                  elementManager;
    private WorkShopJson                    workShopJson;
    private StopWatch                       pageLoad  ;
    private ConnexionPageActions connexionPage;
    private P1Actions p1Page;



    private WebDriver driver;

    public PageObjectManager(WebDriver driver){
        this.driver =   driver;
    }

    public WorkShopJson getWorkShopJson(){return (workShopJson==null)? workShopJson = new WorkShopJson() : workShopJson;}

    public NadaEMailService getNadaEmailService(){return (nadaEmail==null)? nadaEmail = new NadaEMailService() : nadaEmail;}

    public StopWatch  getStopWatch(){ return (pageLoad==null)? pageLoad  = new StopWatch()  :  pageLoad;}

    public ElementManager   getElementManager(){return (elementManager==null)? elementManager   =   new ElementManager(driver) : elementManager;}

    public ConnexionPageActions getConnexionPage(){return (connexionPage==null)? connexionPage = new ConnexionPageActions(driver) : connexionPage;}

    public P1Actions getP1Page(){return (p1Page==null)? p1Page = new P1Actions(driver) : p1Page;}


}
