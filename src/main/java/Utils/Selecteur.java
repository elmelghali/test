package Utils;

import org.openqa.selenium.By;

public class Selecteur {
    private String name;
    private By path;

    public Selecteur(String nom, By chemin){
        this.name = nom;
        this.path = chemin;
    }

    public String getName(){
        return name;
    }

    public By getPath(){
        return path;
    }
}
