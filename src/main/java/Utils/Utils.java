package Utils;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import javax.mail.*;
import javax.mail.search.FlagTerm;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Utils {

    public static String Username;
    public static  String Password;
    public static String mdnTransfere;
    public static String adresseSaisie;
    ArrayList<String> valuesList;
    WebDriver driver;

    public Utils() {
        valuesList = new ArrayList<String>();
    }
    public void selectValueFromTheList(WebElement elementList, String valueSelected) throws InterruptedException{
       int i=0,j,index;
        List<WebElement> values=elementList.findElements(By.tagName("li"));
        for (WebElement value : values) {
            valuesList.add(value.getText());
            i++;
        }

        index=0;
        for (WebElement li : values) {
            System.out.println("index est :" + index);
            System.out.println(valuesList.get(index)+ " " + "Et" + "  " +valueSelected );
            if (valuesList.get(index).contains(valueSelected)) {
                //action.moveToElement(li).build().perform();
                //WebDriverWait wait = new WebDriverWait(driver, 25);
                //action.moveToElement( wait.until(ExpectedConditions.elementToBeClickable(li))).click().perform();
                //li.click();
                //JavascriptExecutor jsDriver = (JavascriptExecutor) driver;
                //jsDriver.executeScript("arguments[0].click();",li);
                li.click();
                Thread.sleep(5000);
                break;}
            index++;
        }

    }

    public  char splitCode(String codeVerification, int rang) throws InterruptedException {
        char code=' ';
        Thread.sleep(1000);
        if(codeVerification!=null && codeVerification.length()!=0) {
            return codeVerification.charAt(rang);
        }

        return code;
    }
    public  String splitSimNumber(String SimNumber, int rang) {
         String partNumber = String.valueOf(SimNumber.charAt(rang));
         partNumber = partNumber.concat(String.valueOf(SimNumber.charAt(rang+1)));
         return partNumber;
    }

    public  String readEmail(String username, String password) {
        String body = null;
        String codeDeVerification = null;
        String host = "imap.gmail.com";
        try {

            // create properties
            Properties properties = new Properties();

            properties.put("mail.imap.host", host);
            properties.put("mail.imap.port", "993");
            properties.put("mail.imap.starttls.enable", "true");
            properties.put("mail.imap.ssl.trust", host);

            Session emailSession = Session.getDefaultInstance(properties);

            // create the imap store object and connect to the imap server
            Store store = emailSession.getStore("imaps");

            store.connect(host, username, password);

            // create the inbox object and open it
            Folder inbox = store.getFolder("Inbox");
            inbox.open(Folder.READ_WRITE);

            // retrieve the messages from the folder in an array and print it
            Message[] messages = inbox.search(new FlagTerm(new Flags(Flags.Flag.SEEN), false));
            System.out.println("messages.length---" + messages.length);

            for (int i = 0, n = messages.length; i < n; i++) {

                Message message = messages[i];
                if (message.getSubject().startsWith("Sandbox")) {
                    message.setFlag(Flags.Flag.SEEN, true);
                    System.out.println("---------------------------------");
                    System.out.println("Email Number " + (i + 1));
                    System.out.println("Subject: " + message.getSubject());
                    System.out.println("From: " + message.getFrom()[0]);
                    System.out.println("Text: " + message.getContent().toString());
                    body = getText(message);
                    System.out.println("Text: " + body);
                    break;
                }
            }
            if (body != null) {
                //Pattern p = Pattern.compile("(\\d{5})<");
                Pattern p = Pattern.compile("(\\d{5})");
                Matcher m = p.matcher(body);
                System.out.println("matcher ==> " + m);
                if (m.find())
                    codeDeVerification = m.group(1);

            }else
                System.out.println("The body is empty");

            inbox.close(false);
            store.close();

        } catch (NoSuchProviderException e) {
            e.printStackTrace();
        } catch (MessagingException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return codeDeVerification;
    }

    private static String getText(Part p) throws
            MessagingException, IOException {
        boolean textIsHtml = false;

        if (p.isMimeType("text/*")) {
            String s = (String)p.getContent();
            textIsHtml = p.isMimeType("text/html");
            return s;
        }

        if (p.isMimeType("multipart/alternative")) {
            // prefer html text over plain text
            Multipart mp = (Multipart)p.getContent();
            String text = null;
            for (int i = 0; i < mp.getCount(); i++) {
                Part bp = mp.getBodyPart(i);
                if (bp.isMimeType("text/plain")) {
                    if (text == null)
                        text = getText(bp);
                    continue;
                } else if (bp.isMimeType("text/html")) {
                    String s = getText(bp);
                    if (s != null)
                        return s;
                } else {
                    return getText(bp);
                }
            }
            return text;
        } else if (p.isMimeType("multipart/*")) {
            Multipart mp = (Multipart)p.getContent();
            for (int i = 0; i < mp.getCount(); i++) {
                String s = getText(mp.getBodyPart(i));
                if (s != null)
                    return s;
            }
        }

        return null;
    }

    public  String getUsername() {
        return Username;
    }

    public  String getPassword() {
        return Password;
    }

    public  void setUsername(String username) { Username = username; }

    public  void setPassword(String password) {
        Password = password;
    }

    public  static String getMdnTransfere() { return mdnTransfere; }

    public static void setMdnTransfere(String mdnTransf) { mdnTransfere = mdnTransf; }

    public  static String getAdresseSaisie() { return adresseSaisie; }

    public  static void setAdresseSaisie(String adresse) { adresseSaisie = adresse; }
}
