package Utils;

import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.TimeUnit;

// PROD EndPoint    ==> https://noor:LicornesNoorInwi2018@noor.ma

// UAT EndPoint     ==> https://noor:LicornesNoorInwi2018@webapp.recette.noor.appstud.com


public class WebDriverManager {
    //initialize webdriver

    private WebDriver driver;

    private DesiredCapabilities capabilities;

    private  WebDriverWait waitDriver;

    public final static int TIMEOUT = 30;
    public final static int PAGE_LOAD_TIMEOUT = 50;

    public WebDriverManager(){
        /*
        System.out.println("start web driver");

        System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir")+"\\src\\test\\resources\\executables\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();

        waitDriver = new WebDriverWait(driver, TIMEOUT);
        driver.manage().timeouts().implicitlyWait(TIMEOUT, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(PAGE_LOAD_TIMEOUT, TimeUnit.SECONDS);
        String window=driver.getWindowHandle();
        System.out.println("Window ->"+window);
        */
        if (driver==null)
          setChromeDriver();
        //if(driver2==null)
            //setChromeDriver();
          //setFirefoxDriver();
        //setExplorerDriver();
        //openPage("https://mail.google.com/mail/u/0/");

    }
    public void setExplorerDriver(){
        System.out.println("start chrome web driver");

        System.setProperty("webdriver.edge.driver",System.getProperty("user.dir")+"/src/test/resources/executables/MicrosoftWebDriver");
        driver = new EdgeDriver();
        driver.manage().window().maximize();

        waitDriver = new WebDriverWait(driver, TIMEOUT);
        driver.manage().timeouts().implicitlyWait(TIMEOUT, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(PAGE_LOAD_TIMEOUT, TimeUnit.SECONDS);
        String window=driver.getWindowHandle();
        System.out.println("Window ->"+window);
    }

    public void setChromeDriver(){
        System.out.println("start chrome web driver");
       // System.setProperty("webdriver.chrome.driver",System.getenv("WEBDRIVER"));
        System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir")+"/src/test/resources/executables/chromedriver");
		//System.setProperty("webdriver.chrome.driver","/usr/local/bin/chromedriver");
   ChromeOptions options = new ChromeOptions();
//options.setExperimentalOption("prefs", chromePrefs);
options.addArguments("--no-sandbox");
options.addArguments("--headless"); //!!!should be enabled for Jenkins
options.addArguments("--disable-dev-shm-usage"); //!!!should be enabled for Jenkins
options.addArguments("--window-size=1920x1080"); //!!!should be enabled for Jenkins

        driver = new ChromeDriver(options);
        driver.manage().window().maximize();

        waitDriver = new WebDriverWait(driver, TIMEOUT);
        driver.manage().timeouts().implicitlyWait(TIMEOUT, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(PAGE_LOAD_TIMEOUT, TimeUnit.SECONDS);

    }

    public void setFirefoxDriver(){
        System.out.println("start firefox web driver");
        //capabilities = DesiredCapabilities.firefox();
        FirefoxOptions options  = new FirefoxOptions();
        options.addPreference("marionette",true);
        options.setLegacy(true);

        //capabilities.setJavascriptEnabled(true);
        //capabilities.setCapability("marionette", true);

        driver  =   new FirefoxDriver();
        //driver = new FirefoxDriver(capabilities);


        //System.out.println("my driver is : "+driver);

        driver.manage().window().maximize();

        //waitDriver = new WebDriverWait(driver, TIMEOUT);

        //driver.manage().timeouts().implicitlyWait(TIMEOUT, TimeUnit.SECONDS);

       // driver.manage().timeouts().pageLoadTimeout(PAGE_LOAD_TIMEOUT, TimeUnit.SECONDS);

        //String window=driver.getWindowHandle();
        //System.out.println("Window ->"+window);

    }

    public void openPage(String url) { ;
        System.out.println(url);
        System.out.println(driver);
        driver.get(url);

       // driver.navigate().to(url);
    }



    public void openAgainWinRecettePage() {
        String httpsUrl="https://webapp.recette.noor.ma/";
        System.out.println(driver);
        driver.get(httpsUrl);
    }

    public WebDriver getDriver() {
        return driver;
    }

    //public void tearDown() {if(driver != null){driver.close();driver.quit();}}
    public void deleteCockies(){
        driver.manage().deleteAllCookies();
    }

    public void waitForLoad() {
        new WebDriverWait(driver, 90).until((ExpectedCondition<Boolean>) wd ->
                ((JavascriptExecutor) wd).executeScript("return document.readyState").equals("complete"));
        System.out.println("page loaded");
    }





}
