package RunCucumberTest.P1;


import RunCucumberTest.defaultRunner;
import cucumber.api.CucumberOptions;
import org.testng.annotations.Factory;

@CucumberOptions(
        monochrome = false,
        plugin = {"pretty", "html:target/Extent_Reports/reports.html","json:target/Extent_Reports/report.json","com.cucumber.listener.ExtentCucumberFormatter:"},
        features =
                   "src/test/resources/Scenarios/Parcours",
        glue = {
                "Steps",
        },tags = { "@P1"
        }
)
public class Runner extends defaultRunner {

        @Factory
        public Object[] factoryMethod() {
                int number_of_times = 1;
                Object[] objects = new Object[number_of_times];

                for (int i = 0; i < number_of_times; i++) {
                        objects[i] = new Runner();
                }
                return objects;
        }

}
