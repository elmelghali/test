package RunCucumberTest;

import Utils.ExtentReports.ExtentManager;
import com.cucumber.listener.ExtentProperties;
import com.cucumber.listener.Reporter;
import cucumber.api.testng.AbstractTestNGCucumberTests;
import org.testng.annotations.*;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class defaultRunner extends AbstractTestNGCucumberTests {

    @BeforeSuite
    public void beforeSuite(){
        System.out.println("before suite");


        //Reporter.
        //ExtentManager.getReporter();
        //ExtentManager.getFile();
    }
    @BeforeClass
    public void beforeClass(){
        System.out.println("before class");
        //ExtentManager.generateReport();
    }

    @AfterClass
    public void afterClass(){
        //ExtentManager.generateReport();
        //Reporter.loadXMLConfig(new File("src/test/resources/extent.xml"));


        Reporter.setSystemInfo("User Name", System.getProperty("user.name"));
        Reporter.setSystemInfo("Time Zone", System.getProperty("user.timezone"));
        Reporter.setSystemInfo("Machine", "Windows 10" + "64 Bit");
        Reporter.setSystemInfo("Selenium", "3.7.0");
        Reporter.setSystemInfo("Maven", "3.5.2");
        Reporter.setSystemInfo("Java Version", "1.8.0_151");
        System.out.println("after class");
    }

    @AfterSuite
    public void afterSuite(){
        //ExtentManager.generateReport();
        System.out.println("after suite");
        System.out.println(ExtentManager.getFile().getPath());
        ExtentProperties extentProperties = ExtentProperties.INSTANCE;
        extentProperties.setReportPath(ExtentManager.getFile().getPath());
        // extent.flush();
        //extent.close();
    }


}
