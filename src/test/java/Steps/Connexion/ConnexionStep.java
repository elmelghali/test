package Steps.Connexion;

import Cucumber.TestContext;
import Pages.Actions.ConnexionPageActions;
import Steps.DefaultStep;
import Utils.Utils;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ConnexionStep extends DefaultStep {
    ConnexionPageActions connexionPage          ;
    TestContext testContext;
    Utils utils;
    WebDriver driver;
    WebDriverWait wait ;

    public ConnexionStep(TestContext context){
        super(context);
        testContext         =   context;
        connexionPage       =   context.getPageObjectManager().getConnexionPage();
        this.driver         =   testContext.getWebDriverManager().getDriver();
        this.wait           =   new WebDriverWait(driver,10);
        utils               =   new Utils();
    }

    @Given("^I m on the page of indh \"([^\"]*)\"$")
    public void i_am_on_the_page_of_indh(String webSiteURL) throws Exception  {
        testContext.getWebDriverManager().openPage(webSiteURL);
        System.out.println("open indh page");
    }

    @Then("^I set the \"([^\"]*)\" user name$")
    public void i_set_user_name(String userName) throws Exception  {
        connexionPage.setUserName(userName);
    }
    @And("^I set the \"([^\"]*)\" password$")
    public void i_set_password(String password) throws Exception  {
        connexionPage.setPassword(password);
    }
    @And("^I click on Connexion Button$")
    public void i_click_on_Connexion_button() throws Exception  {
        connexionPage.connexion();
        Thread.sleep(9000);
    }




}
