package Steps;

import Cucumber.TestContext;


import org.apache.commons.lang3.time.StopWatch;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;



public class DefaultStep {

    TestContext testContext;
    StopWatch   pageLoad;

    public DefaultStep(TestContext context){
        testContext =   context;
        pageLoad    =   context.getPageObjectManager().getStopWatch();

    }


    @BeforeMethod(alwaysRun = true)
    public void startWatch(){
        System.out.println("before step");
        /*
        System.out.println("start stopWatch");
        pageLoad.start();
        System.out.println("hello from pageLoad");
        System.out.println("pageload  "+pageLoad);
        System.out.println(" is started : "+pageLoad.isStarted());
        */
    }


    @AfterMethod(alwaysRun = true)
    public void StopWatch(){
        System.out.println("after step");
        /*
        System.out.println("stop stopWatch");
        pageLoad.stop();

        System.out.println("timer time: ==> "+pageLoad.getTime()+" is stopped : "+pageLoad.isStopped());
        System.out.println(" start time: "+ pageLoad.getStartTime()+"and stop time: "+pageLoad.getTime());
        */
    }



}
