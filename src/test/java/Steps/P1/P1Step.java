package Steps.P1;

import Cucumber.TestContext;
import Pages.Actions.P1.P1Actions;
import Steps.DefaultStep;
import Utils.Utils;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class P1Step extends DefaultStep {

    P1Actions p1Page;
    TestContext testContext;
    Utils utils;
    WebDriver driver;
    WebDriverWait wait ;

    public P1Step(TestContext context){
        super(context);
        testContext         =   context;
        p1Page =   context.getPageObjectManager().getP1Page();
        this.driver         =   testContext.getWebDriverManager().getDriver();
        this.wait           =   new WebDriverWait(driver,10);
        utils               =   new Utils();
    }


    @And("^I click on Suivi d'exécution project$")
    public void i_click_on_Suivi_Execution_project() throws Exception  {
        p1Page.clickOnSuiviExecutionProject();
    }

    @And("^I click on Programme1 Link$")
    public void i_click_on_Programme1_link() throws Exception  {
        p1Page.clickOnProgramme1Link();
    }

    @And("^I'm looking for a project with a validated status$")
    public void i_m_looking_for_a_project_with_a_validated_status() throws Exception  {
        p1Page.searchForaValidatedProject();
    }

    @Then("^I consult the first project of the list$")
    public void i_consult_the_first_project_of_the_list() throws Exception  {
        p1Page.iConsultTheFirstProjectOfTheList();
    }

    @And("^I check the status of the project$")
    public void i_check_the_status_of_the_project() throws Exception  {
        p1Page.iCheckTheStatusOfTheProject();
    }

    @And("^I create a physical follow-up sheet$")
    public void i_create_a_physical_follow_up_sheet() throws Exception  {
        p1Page.create_a_PhysicalFollowUpSheet();
        Thread.sleep(9000);

    }










}
