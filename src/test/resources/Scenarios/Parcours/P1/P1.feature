@P1
Feature: Tests automatisés pour le projet "Programme 1"

    Background:
        Given I m on the page of indh "https://front.indh-qualif.ma"
        Then I set the "admin@indh.ma" user name
        And I set the "123456" password
        And I click on Connexion Button


    @Create-a_new-OG
    Scenario: The administrator creates a new OG
        And I click on Suivi d'exécution project
        Then I click on Programme1 Link
        And I'm looking for a project with a validated status
        Then I consult the first project of the list
        And I check the status of the project
        And I create a physical follow-up sheet


    @Create-a_new-OG1
    Scenario: The administrator creates a new OG
        And I click on Suivi d'exécution project
        Then I click on Programme1 Link
        And I'm looking for a project with a validated status
        Then I consult the first project of the list
        And I check the status of the project
        And I create a physical follow-up sheet







